note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "EiffelStudio test wizard"
	testing: "type/manual"

class
	GAME_TEST_SET

inherit

	EQA_TEST_SET
		redefine
			on_prepare
		end

feature -- Test routines

	test_gutter_game
		do
			roll_many (0, 20)
			assert ("Score is not zero", g.score = 0)
		end

	test_all_ones
		do
			roll_many (1, 20)
			assert ("Score is not 20", g.score = 20)
		end

	test_one_spare
		do
			roll_spare
			g.roll (3)
			roll_many (0, 17)
			assert ("Score is not 16", g.score = 5 + 5 + 3 * 2)
		end

feature {NONE}

	g: GAME

	on_prepare
		do
			create {GAME_IMPLEMENTATION} g
		end

	roll_many (pins: INTEGER; times: INTEGER)
		require
			times_positive: times >= 0
			times_max_20: times <= 20
			pins_positive: pins >= 0
			pins_max_10: pins <= 10
		do
			across
				1 |..| times as j
			loop
				g.roll (pins)
			end
		end

	roll_spare
		do
			g.roll (5)
			g.roll (5)
		end

end
