note
	description: "Summary description for {GAME_IMPLEMENTATION}."
	author: "Mattia Monga"

class
	GAME_IMPLEMENTATION

inherit

	GAME

feature

	score: INTEGER
		do
			Result := res
		end

	roll (pins: INTEGER)
		do
			current_roll := current_roll + 1
			array.put(pins, current_roll)
			
		end

	ended: BOOLEAN
		do
			if current_roll = 20 then
				Result := True
			end
		end

feature {NONE}

	current_roll: INTEGER
	res : INTEGER
	array : ARRAY[INTEGER]
	do
		array.make_empty
	end

invariant

	valid_roll: 0 <= current_roll and current_roll <= 20

end
